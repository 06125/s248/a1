// Adding the users
db.users.insertMany(
    [
    	{
	        "firstName":"Diane",
	        "lastName":"Murphy",
	        "email":"dmurphy@mail.com",
	        "isAdmin": false,
	        "isActive": true,
    	},
    	{
	        "firstName":"Mary",
	        "lastName":"Patterson",
	        "email":"mpatterson@mail.com",
	        "isAdmin": false,
	        "isActive": true,
    	},
    	{
	        "firstName":"Jeff",
	        "lastName":"Firrelli",
	        "email":"jfirreli@mail.com",
	        "isAdmin": false,
	        "isActive": true,
    	},
    	{
	        "firstName":"Gerard",
	        "lastName":"Bondur",
	        "email":"gbondur@mail.com",
	        "isAdmin": false,
	        "isActive": true,
    	},
    	{
	        "firstName":"Pamela",
	        "lastName":"Castillo",
	        "email":"pcastillo@mail.com",
	        "isAdmin": true,
	        "isActive": false,
    	},
    	{
	        "firstName":"George",
	        "lastName":"Vanauf",
	        "email":"gvanauf@mail.com",
	        "isAdmin": true,
	        "isActive": true,
    	}
    ]
)

db.users.find()

// Add the courses
db.courses.insertMany(
	[
		{
			"name":"Professional Development",
			"price":10000.00,
		},
		{
			"name":"Business Processing",
			"price":13000.00,
		}
	]
)


//Adding the enrollees
	/*
		Note: It will say that the script has been executed but no result show. It return no acknowledgement but array has been created with the IDs inside of it.
		Check with Read operation to see that array has been added.
	*/

db.users.find({"lastName":"Murphy"}).forEach(function(doc) {
        db.courses.updateOne(
            {"name":"Professional Development"},
            {$push:{enrollees: {"userID":doc._id}}}
        )
    }
)
db.users.find({"lastName":"Firrelli"}).forEach(function(doc) {
        db.courses.updateOne(
            {"name":"Professional Development"},
            {$push:{enrollees: {"userID":doc._id}}}
        )
    }
)
db.users.find({"lastName":"Bondur"}).forEach(function(doc) {
        db.courses.updateOne(
            {"name":"Business Processing"},
            {$push:{enrollees: {"userID":doc._id}}}
        )
    }
)
db.users.find({"lastName":"Patterson"}).forEach(function(doc) {
        db.courses.updateOne(
            {"name":"Business Processing"},
            {$push:{enrollees: {"userID":doc._id}}}
        )
    }
)
db.courses.find()


// CRUD Operation
	// Get the users who are not administrator
		db.users.find({"isAdmin":false})